<?php

// Main controler of the API 

namespace Marc\RestFulBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

//extends from a special BaseController edited in order to handle easily REST petitions
class ImagesDataController extends FOSRestController
{
	//This action retreives a config CSV file, process it and validates the data
	//At the end returns the images information validated as an array
	//@returnData -> array(title,description,imgUrl)
	public function getImagesAction()
	{
		//gets the img config file using the File transfer Service (located in Services folder)
		$fileTransfer = $this->get('file_transfer');
		$urlToCall = 'https://docs.google.com/spreadsheet/ccc?key=0Aqg9JQbnOwBwdEZFN2JKeldGZGFzUWVrNDBsczZxLUE&single=true&gid=0&output=csv';
		$csvResult = $fileTransfer->getDataFromUrl($urlToCall);

		// do the first exception depending on the curl http result
		if ($csvResult['code'] === 200 )
		{
			//process the CSV file usin the CsvProcesser service (located in Services folder)
			$csvProceser = $this->get('csv_proceser');
			$validImgsArray = $csvProceser->getValidDataFromConfigFile($csvResult['body']);

			//lets the view handle the data.
			$view = $this->view($validImgsArray, 200)
	            		->setFormat('json')
	        		;
	    }
	    //if the httpcode returns anything else than a 200 it means that we didn't found the CSV file
	    else {
	    	$view = $this->view('Config file not found', 404)
	            		->setFormat('json')
	        		;
	    }
        return $this->handleView($view);
	}
}
