<?php

namespace Marc\RestFulBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MarcRestFulBundle:Default:index.html.twig', array('name' => $name));
    }
}
