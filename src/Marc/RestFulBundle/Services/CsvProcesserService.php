<?php

namespace Marc\RestFulBundle\Services;

use Marc\RestFulBundle\Services\CsvProcesserService;
use Marc\RestFulBundle\Services\FileTransferService;
use Marc\RestFulBundle\Services\CacheProcessingService;

class CsvProcesserService 
{
	protected $fileTransferService;
	protected $cacheService;

	public function __construct(FileTransferService $fileTransfer, CacheProcessingService $cacheProcessingService) { 
		$this->fileTransferService = $fileTransfer;
		$this->cacheService = $cacheProcessingService;
	}

    public function getValidDataFromConfigFile($csvAsString) 
    {
    	// explodes the sring by lines
 		$explodedRawCsvString = explode("\n", $csvAsString);
 		$explodedConfigLine = explode(',', $explodedRawCsvString[0]);
 		unset($explodedRawCsvString[0]);
 		$finalImagesArray = array();

 		foreach ($explodedRawCsvString as $csvLine) 
 		{
 			$auxiliarArray = array();
 			$explodedLine = explode(',', $csvLine);
 			foreach ($explodedConfigLine as $key => $configValue) {
 				$auxiliarArray[$configValue] = (isset($explodedLine[$key])) ? $explodedLine[$key] : NULL;
 			}
 			// for each line validates that contains all the data detailed in the header of the CSV file
 			if(!in_array(NULL, $auxiliarArray) && !empty($auxiliarArray["image"])) 
 			{
 				//looks if the image is already in cache
 				if (!$this->cacheService->isImageInCache($auxiliarArray["image"])) 
 				{
 					//validates that the URL corresponds to a real image
	 				$imgCallResult = $this->fileTransferService->getDataFromUrl($auxiliarArray["image"]);
	 				if (FALSE !== strpos($imgCallResult['header'], 'Content-Type: image/png') || (FALSE !== strpos($imgCallResult['header'], 'Content-Type: image/jpeg')) || (FALSE !== strpos($imgCallResult['header'], 'Content-Type: image/gif'))) {
	 					//keeps the valid data
	 					$finalImagesArray[] = $auxiliarArray;
	 					//if is needed we keep a cache history of the image
	 					$cacheResult = $this->cacheService->addImageInCache($auxiliarArray["image"]);
	 				}
	 			}
	 			else {
	 				//keeps the valid data
	 				$finalImagesArray[] = $auxiliarArray;
	 			}
 			}
 		}
 		
		return $finalImagesArray;
    }
}