<?php

namespace Marc\RestFulBundle\Services;

use Marc\RestFulBundle\Services\CacheProcessingService;

class CacheProcessingService 
{
	private $cachePath;

	public function __construct() { 
		$this->cachePath = __DIR__ . '/../Resources/CacheFiles/';
	}

	//in this function it looks if the image is in the cache system.
	public function isImageInCache($imgString)
	{
		$result = false;
		$md5ImgString = md5($imgString);

		if (file_exists($this->cachePath . $md5ImgString)) 
		{
			//I will consider that 1 day will be the limit for storing the information in cache for each image
			$fileCreationTime = filemtime($this->cachePath . $md5ImgString);
			$creationDatetime = new \DateTime(date('Y-m-d H:i:s', $fileCreationTime));
			$cacheLimitDatetime = new \DateTime();
			$cacheLimitDatetime->modify('- 1 days');

			if ($creationDatetime <= $cacheLimitDatetime) {
				$result = true;
			}
		}

		return $result;
	}

	//adds cache for an image
	public function addImageInCache($imgString)
	{
		try {

			$md5ImgString = md5($imgString);
			if (file_exists($this->cachePath . $md5ImgString)) {
				unlink($this->cachePath . $md5ImgString);
			}
			file_put_contents($this->cachePath . $md5ImgString, $imgString);
			$result = array('code' => 200, 'message' => 'ok');

		} catch (\Exception $e) {
			$result = array('code' => 500, 'message' => $e->getMessage());
		}

		return $result;
	}
}