<?php

namespace Marc\RestFulBundle\Services;

use Marc\RestFulBundle\Services\FileTransferService;

class FileTransferService 
{
	public function __construct() { }

    public function getDataFromUrl($urlToCall) 
    {
    	// does a cUrl petition in order to get the contents of the provided URL
 		$ch = curl_init();
 		curl_setopt($ch,CURLOPT_URL,$urlToCall);
 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_HEADER, true);
		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);

		$result = array('code' => $httpCode, 'body' => $body, 'header' => $header);

		// in case that the content has been relocated into another url and is possible to detect 
		// it by the Response headers, calls at the same function recursively
		if ($httpCode == 302 ) {
			$explodedHeaders = explode("\n", $header);
			$relocationUrl = '';
			foreach ($explodedHeaders as $headerData) {
				$explodedLine = explode(' ', $headerData);
				if ($explodedLine[0] == 'Location:') {
					$relocationUrl = $explodedLine[1];
				}
			}

			$result = $this->getDataFromUrl($relocationUrl);
		}

		return $result;
    }
}